<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Ad;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        User::truncate();
        Ad::truncate();

        factory(User::class, 3)
            ->create()
            ->each(function ($user) {
                $quantity = rand(5, 10);
                $user->ads()->saveMany(
                    factory(Ad::class, $quantity)->make()
                );
            });

    }
}
