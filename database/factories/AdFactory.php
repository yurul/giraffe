<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Ad;
use Faker\Generator as Faker;

$factory->define(Ad::class, function (Faker $faker) {

    return [
        'title' => $faker->words(3, true),
        'description' => $faker->paragraph(4)
    ];
});
