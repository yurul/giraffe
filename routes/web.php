<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false
]);

Route::get('/', 'HomeController@index')->name('home');
Route::get('/edit', 'AdController@create');
Route::get('/edit/{id}', 'AdController@edit');
Route::get('/{id}', 'AdController@show');

Route::post('/edit', 'AdController@store');
Route::put('/edit/{id}', 'AdController@update');
Route::delete('/delete/{id}', 'AdController@destroy');
