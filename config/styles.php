<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Classes to style ads
    |--------------------------------------------------------------------------
    */

    'classes' => [
        'pink-post',
        'gray-post',
        'green-post',
        'blue-post',
        'yellow-post'
    ],

    /*
    |--------------------------------------------------------------------------
    | Pins images
    |--------------------------------------------------------------------------
    */

    'pins' => [
        'bluepin.png',
        'bulavka.png',
        'guma.png',
        'guma2.png',
        'iron.png',
        'pinkpin.png',
        'pinyellow.png',
        'pinyellow2.png',
        'redpin.png',
        'redpin2.png',
    ]

];
