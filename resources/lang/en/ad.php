<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'save'=>'SAVE',
    'create' => 'CREATE',
    'edit' => 'EDIT',
    'delete' => 'DELETE',
    'edit_title' => 'Edit your post',
    'create_title' => 'Create new post',


];
