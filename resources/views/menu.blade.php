<nav>
    @inject('request', 'Illuminate\Http\Request')
    <div class="container">
        <div class="row">
            <div class='sitename col-xs-12 col-sm-7'>
                <h1>
                    <b>{{ __('menu.main_title') }}</b>
                </h1>
            </div>
            <div class="create_link col-xs-6 col-sm-3 text-right">
                @auth
                    <h3>
                        <a href="{{ url('edit') }}">
              <span class="{{ $request->path() == 'edit' ? 'active' : '' }}">
                {{ __('menu.create_title') }}
              </span>
                        </a>
                    </h3>
                @endauth
            </div>
            <div class="create_link col-xs-6 col-sm-2 text-right">
                <h3>
                    <a href="{{ url('/') }}">
              <span class="{{ $request->path() == '/' ? 'active' : '' }}">
                {{ __('menu.home') }}
              </span>
                    </a>
                </h3>
            </div>
        </div>
    </div>
</nav>
