@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-2 col-md-3"></div>
            <div class="paper col-sm-8 col-md-6">
                <div>
                    <h2 class="text-center">
                        <b>
                            {{ $ad->title }}
                        </b>
                    </h2>
                    <br/>
                </div>
                <div>
                    {{ $ad->description }}
                </div>
                <div class="adcontent">
                    <br/>
                    <div class="text-left half">{{ $ad->nice_date }}</div>
                    <div class="text-right half">{{ $ad->user->username }}</div>
                    <br/>
                    <br/>
                    @if(Auth::check() && $ad->user_id == Auth::user()->id)
                        <div class="text-right half">
                  <span class="title_link control psevdo_link" href='{{ url("edit/{$ad->id}") }}'
                        onclick='location.href=this.getAttribute("href")'>
                    {{ __('ad.edit') }}
                  </span>
                        </div>
                        <div class="text-left half">
                <span class="control psevdo_link" data-link='{{ url("/delete/{$ad->id}") }}'
                   onclick="delete_ad(this.dataset.link)">{{ __('ad.delete') }}</span>
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-sm-2 col-md-3"></div>
        </div>
    </div>
    <br/><br/>

@endsection
