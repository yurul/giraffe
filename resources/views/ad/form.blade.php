@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-2 col-md-3"></div>
            <div class="col-sm-8 col-md-6">
                @include('errors')
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title title">
                            <b>
                                @if($ad)
                                    {{ __('ad.edit_title') }}
                                @else
                                    {{ __('ad.create_title') }}
                                @endif
                            </b>
                        </h2>
                    </div>
                    <div class="panel-body">
                        <form method="POST" action="{{ $action }}">
                            @csrf
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Title" maxlength="250" name="title"
                                           type="text" value="{{ $ad ? $ad->title : '' }}">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" maxlength="1500" placeholder="Description"
                                              name="description">{{ $ad ? $ad->description : '' }}</textarea>
                                </div>
                                <div>
                                    @if($ad)
                                        <input name="_method" value="PUT" type="hidden">
                                    @endif
                                    <button class="btn btn-lg btn btn-light btn-block form-control" style="color:gray"
                                            type="submit">
                                        {{ $submit }}
                                    </button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <br/><br/>
            </div>
            <div class="col-sm-2 col-md-3"></div>
        </div>
    </div>
    <br/><br/>

@endsection
