<br/>
<div class="container">
    <div class="row">
        @foreach($ads as $ad)
            <div class="post-holder col-sm-6 col-md-4 col-lg-3">
                <div class="post {{ get_random_style() }}">
                    <div class='post-pin'>
                        <img src="{{ asset('images/'.get_random_pin()) }}" alt=""/>
                    </div>
                    <h3 class=""><a class="title_link" href='{{ url("/{$ad->id}") }}'><b>{{ $ad['title'] }} </b></a>
                    </h3>
                    <p class="adcontent">{{ $ad['description'] }}</p>
                    <div class="text-right">{{ $ad->nice_date }}</div>
                    <p class="text-right">{{ $ad->user->username }}</p>
                    @if(Auth::check() && $ad->user_id == Auth::user()->id)
                        <p class="text-left">
                          <span>
                              <a class="title_link control"
                                 href='{{ url("/edit/{$ad->id}") }}'><b>{{ __('ad.edit') }}</b></a>
                              <a class="title_link control psevdo_link" data-link='{{ url("/delete/{$ad->id}") }}'
                                 onclick="delete_ad(this.dataset.link)"><b>{{ __('ad.delete') }}</b>
                              </a>
                          </span>
                        </p>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
    <div class="row">
        {{ $ads->onEachSide(1)->links('vendor.pagination.default') }}
    </div>
</div>
