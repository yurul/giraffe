<div class="container">
    <div class="row">
        <div class="col-sm-3 col-md-4"></div>
        <div class="col-sm-6 col-md-4">

            @include('errors')
            @guest
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title title"><b>{{ __('auth.enter') }}</b></h2>
                    </div>
                    <div class="panel-body">
                        <form method="post" role="form" action="{{ url('/login') }}">
                            @csrf
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="username"
                                           value="{{ old('username') }}" required type="text">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password"
                                           required value="">
                                </div>
                                <div>
                                    <button class="btn btn-lg btn btn-light btn-block form-control text-secondary"
                                            type="submit">{{ __('auth.login') }}</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <br/><br/>
                </div>
            @endguest

            @auth
                <div class="title text-center">
                    <div>
                        <span>{{ __('auth.greetings',['name'=> Auth::user()->username]) }} </span>
                        <br/><br/>
                        <span class="psevdo_link" href="{{ route('logout') }}"
                              onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </span>

                    </div>
                    <form id="logout-form" class="hidden" action="{{ route('logout') }}" method="POST">
                        @csrf
                    </form>
                </div>
            @endauth

        </div>
        <div class="col-sm-3 col-md-4"></div>
    </div>
</div>
<br/><br/>
