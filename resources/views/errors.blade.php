@if ($errors->any())
    <div class="alert alert-danger bigger">
        <div class="text-center">
            <img width="40" height="40" src="{{ asset('images/error.png') }}"/>
        </div>
        <br/>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
