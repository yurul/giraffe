<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <style>
        body {
            background-image: url( {{ asset('images/wood2.jpg') }} )
        }

        @font-face {
            font-family: Eraser;
            src: url( {{ asset('fonts/DSEraserCyr.woff') }} )
        }

        @font-face {
            font-family: Lato;
            src: url( {{ asset('fonts/ComicSansMS.woff') }} )
        }
    </style>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">

    @include('menu')

    <main class="py-4">
        @yield('content')
    </main>
</div>
</body>
</html>
