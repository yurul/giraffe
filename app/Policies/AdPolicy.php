<?php

namespace App\Policies;

use App\User;
use App\Ad;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create ads.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the ad.
     *
     * @param  \App\User $user
     * @param  \App\Ad $ad
     * @return mixed
     */
    public function update(User $user, Ad $ad)
    {
        return $this->isOwner($user, $ad);
    }

    /**
     * Determine whether the user can delete the ad.
     *
     * @param  \App\User $user
     * @param  \App\Ad $ad
     * @return mixed
     */
    public function delete(User $user, Ad $ad)
    {
        return $this->isOwner($user, $ad);
    }

    private function isOwner($user, $resource)
    {
        return $user->id == $resource->user_id;
    }

}
