<?php

namespace App\Http\Controllers;

use App\Ad;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $ads = Ad::orderBy('created_at', 'DESC')->paginate(5);
        return view('home', ['ads' => $ads]);
    }
}
