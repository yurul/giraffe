<?php

namespace App\Http\Controllers;

use App\Ad;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AdController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ad.form', [
            'action' => url('/edit'),
            'ad' => null,
            'submit' => __('ad.create')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        return $this->saveAd($request, new Ad());

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ad $id
     */
    public function show(Ad $id)
    {
        return view('ad.show', ['ad' => $id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ad $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $id)
    {
        $user = Auth::user();
        if (!$user->can('update', $id)) {
            $message = __('auth.unathorized');
            $error_bag = create_err_bag_with_msg($message);
            return redirect('/')->with('errors', $error_bag);
        }

        return view('ad.form', [
            'ad' => $id,
            'action' => url("/edit/{$id->id}"),
            'submit' => __('ad.save')
        ]);
    }

    /**
     * Update the specified resource from storage.
     *
     * @param  \App\Ad $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ad $id)
    {
        $user = Auth::user();
        if (!$user->can('update', $id)) {
            $message = __('auth.unathorized');
            $error_bag = create_err_bag_with_msg($message);
            return redirect('/')->with('errors', $error_bag);
        }
        return $this->saveAd($request, $id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ad $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ad $id)
    {
        $user = Auth::user();
        if (!$user->can('delete', $id)) {
            $message = __('auth.unathorized');
            return abort('403', $message);
        }
        $id->delete();
        return ['redirect' => url('/')];
    }

    private function saveAd($request, $ad)
    {
        $request->validate([
            'title' => 'required|string|max:250',
            'description' => 'required|string|max:1500',
        ]);

        $ad->title = $request->input('title');
        $ad->description = $request->input('description');
        $ad->user_id = auth()->user()->id;

        $ad->save();
        return redirect()->route('home');
    }

}
