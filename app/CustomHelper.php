<?php

if (!function_exists('get_random_style')) {
    function get_random_style()
    {
        $config = config('styles');
        $classes = $config['classes'];
        $random_key = rand(1, count($classes)) - 1;
        return $classes[$random_key];
    }
}

if (!function_exists('get_random_pin')) {
    function get_random_pin()
    {
        $config = config('styles');
        $pins = $config['pins'];
        $random_key = rand(1, count($pins)) - 1;
        return $pins[$random_key];
    }
}

if (!function_exists('create_err_bag_with_msg')) {
    function create_err_bag_with_msg($message)
    {
        $error_bag = new Illuminate\Support\MessageBag;
        $error_bag->add('status', $message);
        return $error_bag;
    }
}
