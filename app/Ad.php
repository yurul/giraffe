<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $table = 'ads';

    protected $fillable = [
        'title',
        'description'
    ];

    protected $visible = [
        'id',
        'title',
        'description'
    ];

    public function getNiceDateAttribute()
    {
        return date("F j, Y, g:i a", strtotime($this->created_at));
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
